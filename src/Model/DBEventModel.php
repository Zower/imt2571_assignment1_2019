<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
      $teller = 0;
      $eventList = array();
      // TODO: Retrive events from the database and add to the list, one by one
      $stmt = $this->db->query('SELECT id, title, date, description FROM event');
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $eventList[$teller] = new Event($row['title'], $row['date'], $row['description'], $row['id']);
          $teller++;
      }
        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {

        if (!is_numeric($id)) {
          throw new InvalidArgumentException("Invalid Parameters");
        }
        $event = null;
        $stmt = $this->db->prepare('SELECT * FROM Event WHERE id = ?');
        $stmt->execute([$id]);


        
        if($stmt->rowCount() > 0) {
          $arr = $stmt->fetch(PDO::FETCH_ASSOC);
          $event = new Event($arr['title'], $arr['date'], $arr['description'], $arr['id']);
        }
        else {
          throw new PDOException("Invalid Parameters");
        }

        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        $event->verify(true);

        if($event->id != -1){
          throw new InvalidArgumentException('ID has to be -1');
        }

        
        $stmt = $this->db->prepare("INSERT INTO event (title, date, description) VALUES (?,?,?)");

        if (!$stmt->execute([$event->title, $event->date, $event->description])){
          throw new PDOException("Invalid parameters");
        }
        $event->id = $this->db->lastInsertId();
        
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        $event->verify(true);
        $stmt = $this->db->prepare("UPDATE event SET title = ?, date = ?, description = ? WHERE id=?");
        if (!$stmt->execute([$event->title, $event->date, $event->description, $event->id])) {
          throw new PDOException("Invalid parameters");
        }
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        if (!is_numeric($id)){
          throw new InvalidArgumentException("Invalid parameters");
        }
        $stmt = $this->db->prepare("DELETE FROM event WHERE id=?");

        $stmt->execute([$id]);
        
    }
}
